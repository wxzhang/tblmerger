from setuptools import find_packages, setup

setup(
    name="tblMerger",
    version="1.0.0",
    packages=find_packages(),
    description="Merge four types of multiple tables",
    long_description="One-click merge four types of numerous tables downloaded from Soil and Water Conservation "
                     "Information System",
    url="https://gitlab.com/wxzhang/tblmerger.git",
    author="Wenxing Zhang",
    author_email="zhangwenxing93@126.com",
    license="MIT",
    python_requires=">=3.6",
    install_requires=["numpy", "pandas", "xlrd", "fire"],
    classifiers=[
        # https://pypi.python.org/pypi?%3Aaction=list_classifiers
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.6",
    ],
    keywords="html-xml converter, table merger",
)
