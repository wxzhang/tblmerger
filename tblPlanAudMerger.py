#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import pandas as pd
import win32com.client
import xlrd
from datetime import datetime
from xlrd import xldate_as_tuple
import fire


headers = ['序号', '项目名称', '水土保持总投资（万元）', '水土保持补偿费（万元）', '防治责任范围（公顷）',
           '减少水土流失量（万吨）', '弃渣量（万立方米）', '编制单位', '受理时间', '评审时间', '批复文号', '批复时间',
           '建设地点', '所属行业', '建设单位', '流域机构', '评审单位', '修改变更']


def extractData(filename):
    """
    从已转换为xls格式的工作簿提取工作表1内除前3行表头以外的有效信息
    :param filename: 工作簿名称
    :return: a list of n lists (n是表单有效信息行数)
    """
    rbook = xlrd.open_workbook(filename)
    sheet = rbook.sheet_by_index(0)  # sheet1
    rows = sheet.nrows
    cols = sheet.ncols
    # print(f"{sheet.name}有{rows}行, 有{cols}列")
    extractedData = []

    # 判断单元格类型作相应处理(ctype: 0 empty, 1 string, 2 number, 3 date, 4 boolean, 5 error)
    for i in range(3, rows):  # 去掉前3行不规则表头
        rowContent = []
        for j in range(cols):
            ctype = sheet.cell(i, j).ctype  # 单元格数据类型
            cell = sheet.cell_value(i, j)
            if ctype == 2 and cell % 1 == 0:  # 如是整数
                cell = int(cell)
            elif ctype == 3:  # 转成datetime
                date = datetime(*xldate_as_tuple(cell, 0))
                cell = date.strftime('%Y/%m/%d')
            rowContent.append(cell)
        extractedData.append(rowContent)
    # print(extractedData)
    return extractedData


def tblPlanAudMerge(startDate, endDate, filePath='./src/src_方案审批情况库/', outputFilename='./dst/方案审批情况汇总.xlsx'):
    """
    :param startDate: 筛选时间段开始日期 (日：2017-1-1)，精确到日
    :param endDate: 筛选时间段结束日期 (日：2019-12-31)，精确到日
    :param filePath: 库：方案审批情况库相对路径,不建议自定
    :param outputFilename: 输出汇总文件名，可自定绝对路径，但由于后续引用不建议自定
    :return: 所有信息整合后的xlsx文件
    """
    # 判断是否存在库
    if not os.path.exists(filePath):
        os.mkdir(filePath)
        exit(f"{filePath}不存在。已建{filePath}，请在库中添加源文件。")

    # 显示库内源文件数量、名称
    fileList = os.listdir(filePath)
    print(f'库:{filePath}包含{len(fileList)}个表')

    # 调用excelVBA
    xlApp = win32com.client.Dispatch("Ket.Application")

    # 根据表头建暂空汇总dataframe
    dfMerged = pd.DataFrame(columns=headers)

    for i in range(len(fileList)):
        oriFile = os.path.join(filePath, fileList[i])
        abspath_oriFile = os.path.abspath(oriFile)

        # 转html-xml格式源文件为xls格式新文件，另存为带“converted”后缀字段的新工作簿
        newFile = os.path.splitext(abspath_oriFile)[0]+'_converted'
        excel = xlApp.Workbooks.Open(abspath_oriFile)  # 注意：VBA只能打开绝对路径
        excel.SaveAs(newFile, FileFormat=56)  # 56: xlExcel8 Excel 97-2003 工作簿 *.xls
        excel.Close()

        # 将某xls文件有效信息提取并转成dataframe
        df = pd.DataFrame(extractData(newFile+'.xls'), columns=headers)

        # 将该dataframe合并到汇总dataframe
        dfMerged = dfMerged.append(df)

    print(f"数据整合后，方案审批情况共有{len(dfMerged)}条有效信息")

    # 将“批复时间”转为datetime格式，用于后续时间范围框选
    dfMerged['批复时间'] = pd.to_datetime(dfMerged['批复时间'], format="%Y/%m/%d")
    dfMerged = dfMerged[(dfMerged['批复时间'] > startDate) & (dfMerged['批复时间'] <= endDate)]
    # 删除无用“序号”列
    dfMerged = dfMerged.drop(['序号'], axis=1)

    # 去除批复时间“时分秒”保留“年月日”
    dfMerged['批复时间'] = dfMerged['批复时间'].dt.strftime('%Y/%m/%d')
    # 将批复时间设置为index，并升序排序
    dfMerged = dfMerged.set_index(['批复时间'])
    dfMerged = dfMerged.sort_index()
    print(f"删去指定时间段（{startDate}-{endDate}）以外的信息，还剩{len(dfMerged)}条有效信息")
    # 保存文件
    dfMerged.to_excel(outputFilename)


if __name__ == '__main__':
    fire.Fire(tblPlanAudMerge)

