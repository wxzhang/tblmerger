import fire
from tblSumInfoMerger import tblSumInfoMerge
from tblCharInfoMerger import tblCharInfoMerge
from tblPlanAudMerger import tblPlanAudMerge
from tblCompFeeMerger import tblCompFeeMerge
import pandas as pd
from collections import Counter


def consistencyCheck(list1, list2):
    check1 = [ele for ele in list1 if ele not in list2]
    check2 = [ele for ele in list2 if ele not in list1]
    if len(check1) == len(check2) == 0:
        # print('生产建设项目字段元素相同，可以直接整合')
        return True
    else:
        print(check1, check2)
        return False


def main(startDate, endDate,):
    print("-----"*9)
    tblPlanAudMerge(startDate=startDate, endDate=endDate)
    print("-----" * 9)
    tblSumInfoMerge()
    print("-----" * 9)
    tblCharInfoMerge()
    print("-----" * 9)
    tblCompFeeMerge()
    print("-----" * 9)
    df_0 = pd.read_excel('./dst/项目综合信息汇总.xlsx', sheet_name=0)
    df_1 = pd.read_excel('./dst/方案特性信息汇总.xlsx', sheet_name=0)
    df_2 = pd.read_excel('./dst/方案审批情况汇总.xlsx', sheet_name=0)
    df_3 = pd.read_excel('./dst/补偿费信息汇总.xlsx', sheet_name=0)
    # 检查每个工作薄中是否有重复的项，输出空字典表示没有重复录入的项。如有重复录入的项则输出字典，需要返回修改汇总表文件。
    repetitionCheck0 = {key: value for key, value in Counter(list(df_0["生产建设项目"])).items() if value > 1}
    repetitionCheck1 = {key: value for key, value in Counter(list(df_1["生产建设项目"])).items() if value > 1}
    repetitionCheck2 = {key: value for key, value in Counter(list(df_2["项目名称"])).items() if value > 1}
    repetitionCheck3 = {key: value for key, value in Counter(list(df_3["生产建设项目"])).items() if value > 1}
    if len(repetitionCheck0) == 0 and len(repetitionCheck1) == 0 and len(repetitionCheck2) == 0 and \
            len(repetitionCheck3) == 0:
        print("四类表均无重复项")
    else:
        print(repetitionCheck0, repetitionCheck1, repetitionCheck2, repetitionCheck3)
        exit("表格数据存在重复项，请重新核查或重新下载表格")

    # 以df_0的“生产建设项目”为关联键，整合df_0和df_1形成df
    if consistencyCheck(list(df_0['生产建设项目']), list(df_1['生产建设项目'])):
        df = pd.merge(df_0, df_1, how='inner', on='生产建设项目', left_index=False, right_index=False, sort=False)
    # 以df的“生产建设项目”为关联键，整合df和df_2更新df
    if consistencyCheck(list(df['生产建设项目']), list(df_2['项目名称'])):
        df = pd.merge(df, df_2, how='inner', left_on='生产建设项目', right_on='项目名称', left_index=False,
                      right_index=False, sort=False)
    # 以df的“生产建设项目”为关联键，整合df和df_3更新df
    if consistencyCheck(list(df['生产建设项目']), list(df_3['生产建设项目'])):
        df = pd.merge(df, df_3, how='inner', on='生产建设项目', left_index=False, right_index=False, sort=False)
    # 保留需要的字段，可自订
    headerstoSave = ['批复时间', '生产建设项目', '受理日期', '管理单位', '所属行业_x', '项目性质', '流域管理机构', '涉及省',
                     '涉及县', '方案上报', '防治责任范围', '防治责任范围（公顷）', '技术审查', '方案批复', '监督检查', '监理',
                     '监测', '验收', '项目建设区（公顷）', '直接影响区（公顷）', '损毁水保设施面积（公顷）', '预测水土流失量（吨）',
                     '措施减少水土流失量（吨）', '项目总投资（万元）', '土建投资（万元）', '水保总投资（万元）', '补偿费（万元）_x',
                     '已收补偿费（万元）_x', '扰动土地治理率（%）', '水土流失总治理度（%）', '土壤流失控制比（%）', '拦渣量（%）',
                     '植被恢复系数（%）', '林草覆盖率（%）', '弃渣量（万立方米）', '编制单位', '评审时间', '批复文号', '建设地点',
                     '建设单位', '评审单位', '修改变更', '计划开工日期', '计划完工日期']
    df_new = df[headerstoSave]
    # 删除重复列
    # df_new = df_new.T.drop_duplicates().T  # 不管用，会删除同内容但不同列名的项。
    
    df_new.to_excel('./dst/最终汇总信息表.xlsx')
    print("最终汇总信息表整合完成")


if __name__ == '__main__':
    fire.Fire(main)




