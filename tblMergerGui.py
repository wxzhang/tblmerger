from tkinter import *
from tkinter import filedialog
from tkinter.filedialog import askdirectory
import time

LOG_LINE_NUM = 0

class Gui():
    def __init__(self, newWindow,):
        self.newWindow = newWindow

    # initialize a window
    def init_window(self):
        self.newWindow.title('全国水土保持监督管理数据整合分析软件_V1.0')
        self.newWindow.geometry('800x600')

        self.path1 = StringVar()
        self.path2 = StringVar()
        self.path3 = StringVar()
        self.path4 = StringVar()
        self.fileSave = StringVar()

        # label
        self.lb1 = Label(self.newWindow, text="项目综合信息库:").grid(row=1, column=0)  # “项目综合信息库”文件夹路径
        self.lb2 = Label(self.newWindow, text="方案特性信息库:").grid(row=2, column=0)  # “方案特性信息库”文件夹路径
        self.lb3 = Label(self.newWindow, text="方案审批情况库:").grid(row=3, column=0)  # “方案审批情况库”文件夹路径
        self.lb4 = Label(self.newWindow, text="补偿费信息库:").grid(row=4, column=0)  # “补偿费信息库”文件夹路径
        self.lb5 = Label(self.newWindow, text="保存位置:").grid(row=8, column=0)  # “保存文件位置”文件夹路径

        # entry
        self.entr1 = Entry(self.newWindow, textvariable=self.path1).grid(row=1, column=1)
        self.entr2 = Entry(self.newWindow, textvariable=self.path2).grid(row=2, column=1)
        self.entr3 = Entry(self.newWindow, textvariable=self.path3).grid(row=3, column=1)
        self.entr4 = Entry(self.newWindow, textvariable=self.path4).grid(row=4, column=1)
        self.entr5 = Entry(self.newWindow, textvariable=self.fileSave).grid(row=8, column=1)

        # button
        self.btn1 = Button(self.newWindow, text="打开", command=self.selectpath1).grid(row=1, column=2)
        self.btn2 = Button(self.newWindow, text="打开", command=self.selectpath2).grid(row=2, column=2)
        self.btn3 = Button(self.newWindow, text="打开", command=self.selectpath3).grid(row=3, column=2)
        self.btn4 = Button(self.newWindow, text="打开", command=self.selectpath4).grid(row=4, column=2)
        self.btn5 = Button(self.newWindow, text="保存", command=self.file2save).grid(row=8, column=2)

        # 运行按钮
        self.runbtn = Button(self.newWindow, txt="一键整合", command=)
        # 写日志按钮
        self.tryinglogging_button = Button(self.newWindow, text="登录日志", bg="lightblue", width=10,
                                              command=self.trying)  # 调用内部方法  加()为直接调用

        self.tryinglogging_button.grid(row=1, column=11)

        # wenbenkuang
        self.log_data_Text = Text(self.newWindow, width=66, height=9)  # 日志框
        self.log_data_Text.grid(row=13, column=0, columnspan=10)

    # functions
    def selectpath1(self,):
        self.path1.set(askdirectory())

    def selectpath2(self,):
        self.path2.set(askdirectory())

    def selectpath3(self,):
        self.path3.set(askdirectory())

    def selectpath4(self,):
        self.path4.set(askdirectory())

    def file2save(self,):
        filename = filedialog.asksaveasfilename(initialdir="/", title="Select file",
                                                filetypes=(("csv 文件", "*.csv"), ("all files", "*.*")))
        self.fileSave.set(filename)

    #获取当前时间
    def get_current_time(self):
        current_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
        return current_time

    #日志动态打印
    def write_log_to_Text(self,logmsg):
        global LOG_LINE_NUM
        current_time = self.get_current_time()
        logmsg_in = str(current_time) +" " + str(logmsg) + "\n"      #换行
        if LOG_LINE_NUM <= 7:
            self.log_data_Text.insert(END, logmsg_in)
            LOG_LINE_NUM = LOG_LINE_NUM + 1
        else:
            self.log_data_Text.delete(1.0, 2.0)
            self.log_data_Text.insert(END, logmsg_in)

    def trying(self):
        self.write_log_to_Text("Coiol:str_trans_to_md5 failed")

def gui_start():
    root = Tk()              #实例化出一个父窗口
    myGui = Gui(root)
    # 设置根窗口默认属性
    myGui.init_window()
    root.mainloop()          #父窗口进入事件循环，可以理解为保持窗口运行，否则界面不展示


if __name__=="__main__":
    gui_start()
