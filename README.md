# tblMerger

Different types of tables that are downloaded from Soil and Water Conservation Information System
can be quickly merged into one single file (or separate files) under your predefined settings on labels and time periods. 

With the scripts, it becomes flexible to deal with a few to thousands of tables within the blink of an eye, thus largely increasing the working
efficiency and avoiding human error.


## Installation

1. Download the code.zip
2. Unzip code.zip somewhere in your pc.
3. In the repo root directory, type the followings in the cmd. After the successful installation, you can apply this 
package anywhere in your pc.
```bash
pip install -e .
```

## Usage
1. Create your working directory, e.g. /Beijing.
2. Make sure you have /src and /dst in /Beijing.
3. Add the files you download from the website to the corresponding /src/src_xxxx folder. (src_xxxx subfolders are created 
by users or can be automatically created through running separate functions)
4. To run the most commonly used function "tblMain" that handles everything into one lumped file, type the followings in  the cmd.
```bash
python -m tblMain --startDate '2017-01-01' --endDate '2019-12-31'
python -m tblMain '2017-01-01' '2019-12-31'
```

5. To run the other functions that handle one specific type of tables, type the followings in the cmd
```bash
python -m tblPlanAudMerger --startDate '2017-01-01' --endDate '2019-12-31'
python -m tblPlanAudMerger '2017-01-01' '2019-12-31'
python -m tblCompFeeMerger
python -m tblSumInfoMerger
python -m tblCharInfoMerger
```

6. tblPlanAudMerger corresponds with “方案审批情况”表
   tblSumInfoMerger corresponds with “项目综合信息”表
   tblCharInfoMerger corresponds with “方案特性信息”表
   tblCompFeeMerger corresponds with “补偿费信息”表
   tblMain creates “最终汇总信息”表
   
7. When rerunning the scripts, remember to clean all *_converted.xls files.