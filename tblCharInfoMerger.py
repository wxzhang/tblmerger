#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import pandas as pd
import win32com.client
import xlrd
from datetime import datetime
from xlrd import xldate_as_tuple
import fire

headers = ['序号', '生产建设项目', '管理单位', '所属行业', '项目性质', '流域管理机构', '批复时间', '项目建设区（公顷）',
           '直接影响区（公顷）', '损毁水保设施面积（公顷）', '预测水土流失量（吨）', '措施减少水土流失量（吨）',
           '弃渣量（方）', '项目总投资（万元）', '土建投资（万元）', '水保总投资（万元）', '补偿费（万元）',
           '已收补偿费（万元）', '扰动土地治理率（%）', '水土流失总治理度（%）', '土壤流失控制比（%）', '拦渣量（%）',
           '植被恢复系数（%）', '林草覆盖率（%）']


def extractData(filename):
    """
    从已转换为xls格式的工作簿提取工作表1内除前6行表头以外的有效信息
    :param filename: 工作簿名称
    :return: a list of n lists (n是表单有效信息行数)
    """
    rbook = xlrd.open_workbook(filename)
    sheet = rbook.sheet_by_index(0)  # sheet1
    rows = sheet.nrows
    cols = sheet.ncols
    # print(f"{sheet.name}有{rows}行, 有{cols}列")
    extractedData = []

    # 判断单元格类型作相应处理(ctype: 0 empty, 1 string, 2 number, 3 date, 4 boolean, 5 error)
    for i in range(6, rows):  # 去掉前6行不规则表头
        rowContent = []
        for j in range(cols):
            ctype = sheet.cell(i, j).ctype  # 单元格数据类型
            cell = sheet.cell_value(i, j)
            if ctype == 2 and cell % 1 == 0:  # 如是整数
                cell = int(cell)
            elif ctype == 3:  # 转成datetime
                date = datetime(*xldate_as_tuple(cell, 0))
                cell = date.strftime('%Y/%m/%d')
            rowContent.append(cell)
        extractedData.append(rowContent)
    # print(extractedData)
    return extractedData


def tblCharInfoMerge(filePath='./src/src_方案特性信息库/', outputFilename='./dst/方案特性信息汇总.xlsx'):
    """
    :param filePath: 方案特性信息库相对路径,不建议自定
    :param outputFilename: 输出汇总文件名，可自定绝对路径，但由于后续引用不建议自定
    :return: 所有信息整合后的xlsx文件
    """
    # 判断是否存在库
    if not os.path.exists(filePath):
        os.mkdir(filePath)
        exit(f"{filePath}不存在。已建{filePath}，请在库中添加源文件。")

    # 显示库内源文件数量、名称
    fileList = os.listdir(filePath)
    print(f'库:{filePath}包含{len(fileList)}个表')

    # 调用excelVBA
    xlApp = win32com.client.Dispatch("Ket.Application")

    # 根据表头建暂空汇总dataframe
    dfMerged = pd.DataFrame(columns=headers)

    for i in range(len(fileList)):
        oriFile = os.path.join(filePath, fileList[i])
        abspath_oriFile = os.path.abspath(oriFile)
        # 转html-xml格式源文件为xls格式新文件，另存为带“converted”后缀字段的新工作簿
        newFile = os.path.splitext(abspath_oriFile)[0]+'_converted'
        excel = xlApp.Workbooks.Open(abspath_oriFile)  # 注意：VBA只能打开绝对路径
        excel.SaveAs(newFile, FileFormat=56)  # 56: xlExcel8 Excel 97-2003 工作簿 *.xls
        excel.Close()
        # 将某xls文件有效信息提取并转成dataframe
        df = pd.DataFrame(extractData(newFile+'.xls'), columns=headers)
        # 将该dataframe合并到汇总dataframe
        dfMerged = dfMerged.append(df)

    # 将“批复时间”转为datetime格式
    dfMerged['批复时间'] = pd.to_datetime(dfMerged['批复时间'], format="%Y/%m/%d")
    # 去除批复时间“时分秒”保留“年月日”
    dfMerged['批复时间'] = dfMerged['批复时间'].dt.strftime('%Y/%m/%d')
    # 删除无用“序号”列
    dfMerged = dfMerged.drop(['序号'], axis=1)

    # 将批复时间设置为index，并升序排序
    dfMerged = dfMerged.set_index(['批复时间'])
    dfMerged = dfMerged.sort_index()
    print(f"数据整合后，方案特性信息共有{len(dfMerged)}条有效信息")
    dfMerged.to_excel(outputFilename)


if __name__ == '__main__':
    fire.Fire(tblCharInfoMerge)


